package `in`.co.example.myapplication.walletsection.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


@Parcelize
class CurrentBalance(
    @SerializedName("balance")
    @Expose
    var balance: String? = null,
    @SerializedName("currency")
    @Expose
    var currency: String? = null
) : Parcelable