package com.example.schooldemoform.schoolsection.adapter

import android.graphics.Color
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputLayout

object CustomBindingAdapter {

    @BindingAdapter("app:errorText")
    @JvmStatic
    fun setErrorMessage(view: TextInputLayout, errorMessage: String?) {

        if (errorMessage.isNullOrEmpty()){
            view.isErrorEnabled = false
            view.error = ""
        } else
        view.error = errorMessage
    }




    @BindingAdapter("app:shouldChangeTextViewColor")
    @JvmStatic
    fun changeTextViewColor(view: TextView, isError: Boolean){

        if (isError){
            view.setTextColor(Color.RED)
            view.error = "Select one"

        }else {
            view.setTextColor(Color.BLACK)
            view.error = null
        }
    }
}