package `in`.co.example.myapplication.coremodule.Utils

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import com.example.schooldemoform.MyApplicationClass

class GlobalHealper {



   open fun getLoginHeader():Map<String,String>{

        val map = HashMap<String,String>()

        map.put("Accept","application/json")
        map.put("Content-Type","application/json")

        return map
    }

     open fun getTokenHeader():Map<String,String>{

         var token = SharePrefUtil().getSharePreferenceValue(MyApplicationClass.appContext.applicationContext,SharePrefUtil().TOKEN_KEY)

         val map = HashMap<String,String>()

         map.put("Authorization","Bearer "+token)
         map.put("Content-Type","application/json")

         return map
     }

    fun showConfirmationDialog(context: Context?, title: String, text: String, positiveButtonClickListener: DialogInterface.OnClickListener) {
        // Open the dialog with choices
        val dialog1 = context?.let {
            AlertDialog.Builder(it)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton("YES", positiveButtonClickListener)
                .setNegativeButton("No") { dialog, which -> dialog.dismiss() }
                .create()
        }

        dialog1?.show()
    }

    //hide keyboard
    fun hideKeyboard(ctx: Context) {
        val inputManager = ctx.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        // check if no view has focus:
        val v = (ctx as Activity).currentFocus ?: return

        inputManager.hideSoftInputFromWindow(v.windowToken, 0)
    }
}