package `in`.co.example.myapplication.walletsection.repo

import `in`.co.example.myapplication.coremodule.apiclient.AppNetwork
import `in`.co.example.myapplication.loginsection.model.LoginResponse
import `in`.co.example.myapplication.walletsection.model.CurrentBalance
import `in`.co.example.myapplication.walletsection.model.SpenditureDetail
import `in`.co.example.myapplication.walletsection.model.TransactionDetail
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WalletRepo {


    private val walletServices : WalletServices? = AppNetwork.getRetrofitInstance()?.create(WalletServices::class.java)



    fun getCurrentBalance(map :Map<String,String>,data : MutableLiveData<CurrentBalance>){
//        var data = MutableLiveData<CurrentBalance>()
       walletServices?.getCurrentBalance(map)?.enqueue(object : Callback<CurrentBalance> {

           override fun onResponse(call: Call<CurrentBalance>, response: Response<CurrentBalance>) {

               data.value = response.body()
           }

           override fun onFailure(call: Call<CurrentBalance>, t: Throwable) {
               // TODO better error handling in part #2 ...
               data.value=null
           }
       })


    }


    fun getTransactionHistory(map :Map<String,String>,data:  MutableLiveData<List<TransactionDetail>>){
       // var data = MutableLiveData<List<TransactionDetail>>()

        walletServices?.getTransactionHistory(map)?.enqueue(object : Callback<List<TransactionDetail>> {

            override fun onResponse(call: Call<List<TransactionDetail>>, response: Response<List<TransactionDetail>>) {

                data.value = response.body()
            }

            override fun onFailure(call: Call<List<TransactionDetail>>, t: Throwable) {
                // TODO better error handling in part #2 ...
                data.value=null
            }
        })

    }


    fun saveExpenditure(map :Map<String,String>,spenditureDetail: SpenditureDetail, isErrorOnSave : MutableLiveData<Boolean>){

        walletServices?.saveSpenditure(map,spenditureDetail)?.enqueue(object : Callback<SpenditureDetail> {

            override fun onResponse(call: Call<SpenditureDetail>, response: Response<SpenditureDetail>) {
                isErrorOnSave.value = !response.isSuccessful
            }

            override fun onFailure(call: Call<SpenditureDetail>, t: Throwable) {
                // TODO better error handling in part #2 ...
                isErrorOnSave.value= true
            }
        })

    }
}