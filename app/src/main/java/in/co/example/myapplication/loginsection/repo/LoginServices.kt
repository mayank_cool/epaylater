package `in`.co.example.myapplication.loginsection.repo

import `in`.co.example.myapplication.loginsection.model.LoginInfo
import `in`.co.example.myapplication.loginsection.model.LoginResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.HeaderMap
import retrofit2.http.POST

interface LoginServices {

    @POST("login")
    fun login(@HeaderMap map: Map<String,String>, @Body  loginInfo :LoginInfo ): Call<LoginResponse>
}