package `in`.co.example.myapplication.walletsection.model


import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
 class TransactionDetail(
    @SerializedName("id")
    @Expose
    var id :Int?= 0,
    @SerializedName("date")
    @Expose
    var date:String ?=null ,
    @SerializedName("description")
    @Expose
     var description : String? = null,
    @SerializedName("amount")
    @Expose
     var amount :String? = null,
    @SerializedName("currency")
    @Expose
     var currency :String? = null ): Parcelable