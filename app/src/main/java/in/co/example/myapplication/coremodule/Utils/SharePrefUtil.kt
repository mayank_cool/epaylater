package `in`.co.example.myapplication.coremodule.Utils

import android.content.Context

class SharePrefUtil {

     val TOKEN_KEY = "token"

    fun setSharePreferenceValue(context: Context?, key: String, value: String) {
         context?.getSharedPreferences("settingsPrefs",Context.MODE_PRIVATE)?.edit()?.putString(key, value)?.commit()

    }

    fun getSharePreferenceValue(context: Context, key: String): String? {
        return context.getSharedPreferences("settingsPrefs",Context.MODE_PRIVATE).getString(key, "")
    }
}