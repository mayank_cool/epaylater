package `in`.co.example.myapplication.loginsection.viewmodel

import `in`.co.example.myapplication.coremodule.Utils.GlobalHealper
import `in`.co.example.myapplication.loginsection.model.LoginInfo
import `in`.co.example.myapplication.loginsection.model.LoginResponse
import `in`.co.example.myapplication.loginsection.repo.LoginRepo
import android.util.Log
import android.view.View
import androidx.lifecycle.*
import kotlinx.coroutines.launch

class LoginViewModel : ViewModel() {

    var loginResponse = MutableLiveData<LoginResponse>()
    val  loginRepo =  LoginRepo()
    val loginInfo = LoginInfo()
    var errorMsgForName =  MutableLiveData<String>()
    var errorMsgForPassword =  MutableLiveData<String>()
    var showLoader =  MutableLiveData<Boolean>()



    fun onLoginBtnClick(view : View){

        viewModelScope.launch{

            var goodToGo = true

            goodToGo = validateName(loginInfo.name) && goodToGo
            goodToGo = validatePassword(loginInfo.password) && goodToGo

            if (goodToGo){
                showLoader.value = true
                loginRepo.login(loginInfo,   GlobalHealper().getLoginHeader(),loginResponse)
            }

        }
    }


    fun validateName(text:String ?) : Boolean{

        val error = checkValueIsEmpty(text)

        if (error){
            errorMsgForName.value = "Cannot be empty"
            return false
        }else{
            errorMsgForName.value = ""
            return  true
        }
    }

    fun validatePassword(text:String ?) : Boolean{

        val error = checkValueIsEmpty(text)

        if (error){
            errorMsgForPassword.value = "Cannot be empty"
            return false
        }else{
            errorMsgForPassword.value = ""
            return  true
        }
    }

    fun checkValueIsEmpty(str: String?):Boolean{

        return if (str.isNullOrEmpty()) true else false
    }

}