package `in`.co.example.myapplication.coremodule.apiclient

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

class AppNetwork {



    init {


    }

    companion object {
        private var mRetrofit: Retrofit? = null
        private val HTTPS_BASE_URL = "https://interviewer-api.herokuapp.com/"

        fun getRetrofitInstance(): Retrofit? {

            if (mRetrofit == null){

//                var  httpClient  =  OkHttpClient()
//                httpClient.networkInterceptors().add(Interceptor { chain ->
//                    val request = chain.request().newBuilder()
//                        .addHeader("Accept", "application/json")
//                        .addHeader("Content-Type", "application/json")
//
//                        .build()
//
//                    chain.proceed(request)
//                })

                mRetrofit = Retrofit.Builder()
                    .baseUrl(HTTPS_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return mRetrofit
        }


    }

//    fun getRetrofitInstance(): Retrofit? {
//
//        var  httpClient  =  OkHttpClient()
////        httpClient.networkInterceptors().add( Interceptor() {chain: Interceptor.Chain? ->  })
//        httpClient.networkInterceptors().add(Interceptor { chain ->
//            val request = chain.request().newBuilder()
//                .addHeader("Accept", "application/json")
//                .addHeader("Content-Type", "application/json")
//                .addHeader("Authorization", "")
//                .build()
//
//            chain.proceed(request)
//        })
//
//        if (mRetrofit == null){
//              mRetrofit = Retrofit.Builder()
//                  .baseUrl(HTTPS_BASE_URL)
//                  .client(httpClient)
//                  .addConverterFactory(GsonConverterFactory.create())
//                  .build()
//          }
//
//
//
//
//
//        return mRetrofit
//    }
}