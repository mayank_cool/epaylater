package `in`.co.example.myapplication.walletsection.ui

import `in`.co.example.myapplication.coremodule.Utils.GlobalHealper
import `in`.co.example.myapplication.databinding.CreateSpendFragmentBinding
import `in`.co.example.myapplication.walletsection.viewmodel.CreateSpendViewModel
import `in`.co.example.myapplication.walletsection.viewmodel.CurrentBalanceViewModel
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

class CreateSpendFragment : Fragment() {

     private lateinit var binding : CreateSpendFragmentBinding
     private lateinit var viewModel : CreateSpendViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = CreateSpendFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CreateSpendViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        viewModel.isErrorOnSave.observe(viewLifecycleOwner, Observer {

              if (!it) findNavController().popBackStack()
        })

        viewModel.isCancelBtnPressed.observe(viewLifecycleOwner, Observer {
            this@CreateSpendFragment.context?.let { GlobalHealper().hideKeyboard(it) }
             if (it){
                 viewModel.isCancelBtnPressed.value =false
                 performBackAction()
             }
        })

        // show alert Message
        viewModel.alertMessage.observe(this, Observer {

            this@CreateSpendFragment.context?.let { GlobalHealper().hideKeyboard(it) }
            GlobalHealper().showConfirmationDialog(this@CreateSpendFragment.context, "Alert", it, DialogInterface.OnClickListener { dialogInterface, i ->
                performBackAction()
                dialogInterface.dismiss()
            })
        })
    }


    private fun performBackAction(){

        findNavController().popBackStack()
    }
}