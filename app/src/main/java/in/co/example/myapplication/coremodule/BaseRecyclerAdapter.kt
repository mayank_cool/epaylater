package `in`.co.example.myapplication.coremodule

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerAdapter <ItemType> : RecyclerView.Adapter<BaseRecyclerAdapter<ItemType>.Holder>  {

    protected var list: List<ItemType>? = null


    protected abstract val layout: Int

    constructor(list: List<ItemType>?) {
        this.list = list
    }




    @JvmOverloads
    fun refreshData(list: List<ItemType>, refreshList: Boolean = true) {
        this.list = list
        if (refreshList)
            notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(parent.context), layout, parent, false)
        return Holder(binding)
    }

    override fun getItemCount(): Int {
        return if (list == null) 0 else list!!.size
    }

    inner class Holder(var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root)


}