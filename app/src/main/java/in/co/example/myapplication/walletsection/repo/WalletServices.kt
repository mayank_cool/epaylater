package `in`.co.example.myapplication.walletsection.repo

import `in`.co.example.myapplication.walletsection.model.CurrentBalance
import `in`.co.example.myapplication.walletsection.model.SpenditureDetail
import `in`.co.example.myapplication.walletsection.model.TransactionDetail
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.POST

interface WalletServices {


    @GET("balance")
    fun getCurrentBalance(@HeaderMap map: Map<String,String> ): Call<CurrentBalance>

    @GET("transactions")
    fun getTransactionHistory(@HeaderMap map: Map<String,String> ): Call<List<TransactionDetail>>

    @POST("spend")
    fun saveSpenditure(@HeaderMap map: Map<String,String> ,@Body spenditureDetail: SpenditureDetail): Call<SpenditureDetail>
}