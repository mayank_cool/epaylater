package `in`.co.example.myapplication.walletsection.viewmodel

import `in`.co.example.myapplication.coremodule.Utils.GlobalHealper
import `in`.co.example.myapplication.walletsection.model.CurrentBalance
import `in`.co.example.myapplication.walletsection.model.TransactionDetail
import `in`.co.example.myapplication.walletsection.repo.WalletRepo
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class CurrentBalanceViewModel : ViewModel() {


    var isNeededToShowTitle  = MutableLiveData<Boolean>()
    var currentBalanceString =  MutableLiveData<String>()
    var isFabButtonClicked  = MutableLiveData<Boolean>()
    var showLoader =  MutableLiveData<Boolean>()

    private val  walletRepo = WalletRepo()


    fun getCurrentBalance():LiveData<CurrentBalance>{

        var data = MutableLiveData<CurrentBalance>()
        viewModelScope.launch{

            walletRepo.getCurrentBalance(GlobalHealper().getTokenHeader(),data)
        }

        return data
    }

    fun getTransactionList(): LiveData<List<TransactionDetail>>? {

         showLoader.value = true
        var data = MutableLiveData<List<TransactionDetail>>()
        viewModelScope.launch {
            walletRepo.getTransactionHistory(GlobalHealper().getTokenHeader(),data)
        }

        return data
    }


    fun onFabBtnClick(view :View){

       isFabButtonClicked.value = true
   }

}