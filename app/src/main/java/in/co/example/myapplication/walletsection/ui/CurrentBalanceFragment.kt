package `in`.co.example.myapplication.walletsection.ui

import `in`.co.example.myapplication.R
import `in`.co.example.myapplication.databinding.CurrentBalanceFragmentBinding
import `in`.co.example.myapplication.walletsection.adapter.TransactionAdapter
import `in`.co.example.myapplication.walletsection.viewmodel.CurrentBalanceViewModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.google.android.material.appbar.AppBarLayout

class CurrentBalanceFragment : Fragment() {




    private lateinit var binding : CurrentBalanceFragmentBinding
    private lateinit var viewModel : CurrentBalanceViewModel
    private lateinit var adapter : TransactionAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = CurrentBalanceFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(CurrentBalanceViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        initListener()
        initAdapter()
        renderTransactionList()
    }


    private fun initListener(){



        binding.appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            internal var isShow = false
            internal var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = false
                    binding.isAppBarIsShowing = false
                    binding.titleForShow = " "


                } else if (!isShow) {
                    //mCollapsingToolbarLayout.setTitle("Title")
                    isShow = true
                    binding.isAppBarIsShowing = true
                    binding.titleForHide = viewModel.currentBalanceString.value
                }
            }
        })

        viewModel.getCurrentBalance().observe(viewLifecycleOwner, Observer {

               viewModel.currentBalanceString.value = "ePayLater Wallet: "+it.balance + " " + it.currency
        })

        viewModel.isFabButtonClicked.observe(viewLifecycleOwner, Observer {

                 if (it) {
                     viewModel.isFabButtonClicked.value = false
                     Navigation.findNavController(binding.root)
                         .navigate(R.id.action_currentBalanceFragment_to_CreateSpendFragment)
                 }
        })
    }

   private  fun initAdapter(){
        adapter = TransactionAdapter(null)
        binding.adapter = adapter

    }

   private fun renderTransactionList(){
        viewModel.getTransactionList()!!.observe(viewLifecycleOwner , Observer {
            viewModel.showLoader.value = false
            if (it != null && it.size > 0){
                binding.adapter?.refreshData(it,true)
                binding.isEmptyList = false
            }else{
                binding.isEmptyList = true
            }
        })
    }


}