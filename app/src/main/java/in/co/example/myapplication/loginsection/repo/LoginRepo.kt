package `in`.co.example.myapplication.loginsection.repo

import `in`.co.example.myapplication.coremodule.apiclient.AppNetwork
import `in`.co.example.myapplication.loginsection.model.LoginInfo
import `in`.co.example.myapplication.loginsection.model.LoginResponse
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginRepo  {


    private val loginServices : LoginServices? = AppNetwork.getRetrofitInstance()?.create(LoginServices::class.java)


    fun login(loginInfo: LoginInfo, map :Map<String,String>, data: MutableLiveData<LoginResponse>): MutableLiveData<LoginResponse> {
        loginServices?.login(map,loginInfo)?.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {

                var loginResponse:  LoginResponse = response.body()
                data.value = loginResponse
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                // TODO better error handling in part #2 ...
                data.value=null
            }
        })

        return data
    }
}