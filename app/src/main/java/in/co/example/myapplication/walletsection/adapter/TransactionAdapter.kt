package `in`.co.example.myapplication.walletsection.adapter

import `in`.co.example.myapplication.R
import `in`.co.example.myapplication.coremodule.BaseRecyclerAdapter
import `in`.co.example.myapplication.databinding.AdapterTransactionItemBinding
import `in`.co.example.myapplication.walletsection.model.TransactionDetail

class TransactionAdapter ( private val transactionList: List<TransactionDetail>?) : BaseRecyclerAdapter<TransactionDetail>(transactionList) {


    override val layout: Int
        get() = R.layout.adapter_transaction_item



    override fun onBindViewHolder(holder: BaseRecyclerAdapter<TransactionDetail>.Holder, position: Int) {
        val binding : AdapterTransactionItemBinding = holder.binding as AdapterTransactionItemBinding
        //binding.adapter = this
        var transactionDetail = list?.get(position)
        binding.transactionDetail = transactionDetail
        binding.date = replaceString(transactionDetail?.date)
        binding.spendMoney = transactionDetail?.amount + " "+transactionDetail?.currency
        binding.imageText = transactionDetail?.description?.get(0)?.toString()

    }


    fun replaceString(date1: String?): String {

        return date1?.replace("T", " ")?.replace("Z", "").toString()
    }

}