package `in`.co.example.myapplication.loginsection.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class LoginResponse(

    @SerializedName("token")
    @Expose
    var token: String? = null ) : Parcelable