package `in`.co.example.myapplication.loginsection.ui

import `in`.co.example.myapplication.R
import `in`.co.example.myapplication.coremodule.Utils.SharePrefUtil
import `in`.co.example.myapplication.databinding.LoginFragmentBinding
import `in`.co.example.myapplication.loginsection.viewmodel.LoginViewModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation


class LoginFragment :Fragment() {


    private lateinit var viewModel: LoginViewModel

    private lateinit var binding : LoginFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = LoginFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        viewModel.loginResponse.observe(this, Observer {

            viewModel.showLoader.value = false
             val token :String? = it.token
               // if (!it.token.isNullOrEmpty()){
                    token?.let { it1 ->
                        SharePrefUtil().setSharePreferenceValue(this.context,SharePrefUtil().TOKEN_KEY, it1)
                        Navigation.findNavController(binding.root).navigate(R.id.action_login_to_current_balance_frag)

                    }
               // }
        })


    }
}