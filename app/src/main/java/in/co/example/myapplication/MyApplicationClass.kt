package com.example.schooldemoform

import android.app.Application
import android.content.Context

class MyApplicationClass : Application() {


    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
    }

    companion object {

        lateinit  var appContext: Context

    }
}