package `in`.co.example.myapplication.walletsection.model

data class SpenditureDetail(

    var amount :String ? = null,
    var description : String? = null,
    var date :String? = null,
    var currency :String? = null
)