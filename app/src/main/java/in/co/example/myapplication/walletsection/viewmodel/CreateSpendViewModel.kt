package `in`.co.example.myapplication.walletsection.viewmodel

import `in`.co.example.myapplication.R
import `in`.co.example.myapplication.coremodule.Utils.GlobalHealper
import `in`.co.example.myapplication.walletsection.model.SpenditureDetail
import `in`.co.example.myapplication.walletsection.repo.WalletRepo
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.schooldemoform.MyApplicationClass
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.ZonedDateTime.*
import java.util.*


class CreateSpendViewModel : ViewModel() {

    var errorMsgForAmount =  MutableLiveData<String>()
    var errorForDescription =  MutableLiveData<String>()
    var speditureDetail = SpenditureDetail()
    var showLoader =  MutableLiveData<Boolean>()
    var isErrorOnSave = MutableLiveData<Boolean>()
    private val  walletRepo = WalletRepo()
    var alertMessage  = MutableLiveData<String>()
    var isCancelBtnPressed  = MutableLiveData<Boolean>()



    fun onSaveBtnClick(view: View){

        viewModelScope.launch{

            var goodToGo = true

            goodToGo = validateAmount(speditureDetail.amount) && goodToGo
            goodToGo = validateDescription(speditureDetail.description) && goodToGo

            if (goodToGo){
                showLoader.value = true
                speditureDetail.date = getCurrentDateAndTime()
                speditureDetail.currency = "GBP"
                Log.e("TAG","Check Date ==> " +  speditureDetail.date)
                walletRepo.saveExpenditure(GlobalHealper().getTokenHeader(),speditureDetail, isErrorOnSave)
            }

        }
    }


    fun onbtnCancelClick(view: View){


       if (!speditureDetail.amount.isNullOrEmpty() || !speditureDetail.description.isNullOrEmpty()){

           // show alert msg to user
           alertMessage.value  = MyApplicationClass.appContext.getString(R.string.cancel_msg_for_edit)
       }else{

           isCancelBtnPressed.value =  true
       }
    }



   private fun getCurrentDateAndTime() : String{


     //  val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
       val df = SimpleDateFormat("yyyy-MM-dd HH:mm:SSS")
       val date :String  = df.format(Calendar.getInstance().getTime())

       return date
   }

    fun validateAmount(text:String ?) : Boolean{

        val error = checkValueIsEmpty(text)

        if (error){
            errorMsgForAmount.value = "Cannot be empty"
            return false
        }else{
            errorMsgForAmount.value = " "
            return  true
        }
    }


    fun validateDescription(text:String ?) : Boolean{

        val error = checkValueIsEmpty(text)

        if (error){
            errorForDescription.value = "Cannot be empty"
            return false
        }else{
            errorForDescription.value = " "
            return  true
        }
    }

    fun checkValueIsEmpty(str: String?):Boolean{

        return if (str.isNullOrEmpty()) true else false
    }
}